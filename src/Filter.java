import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Filter {

    private static String deletedSC = "./deletedSC.txt";
    private static String deletedComm = "./deletedComm.txt";
    private static String selectedComm="./selectedComm.txt";

    private static String[] dislikedComm = {"\"rcu_sched\""};
    private static String[] dislikedSC = {"kmem_kfree","kmem_kmalloc","kmem_cache_free","kmem_cache_alloc",
            "kmem_mm_page_free",
                                        "kmem_mm_page_alloc","kmem_mm_page_alloc_zone_locked","kmem_mm_page_free_batched",
                                        "mm_page_free","mm_page_alloc","mm_page_free_batched","mm_page_alloc_zone_locked","mm_page_pcpu_drain","kmem_mm_page_pcpu_drain"};

    /*
    * remove disliked SC by name
    * */
    public static void removeSC(String inputFile){
        BufferedReader br = null;
        BufferedWriter bw = null;
        Set<String> baselineSet = null;
        try{
            br = new BufferedReader(new FileReader(inputFile));
            bw = new BufferedWriter(new FileWriter(deletedSC));
            baselineSet = new HashSet<>(Arrays.asList(dislikedSC));
            String line = null;
            ArrayList<String> sc_params = null;
            while ((line=br.readLine())!=null){
                sc_params = FileUtils.getNameAndParams(line);
                if (!baselineSet.contains(sc_params.get(0))){
                    bw.write(line+"\n");
                }
            }
            br.close();
            bw.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("success output : "+deletedSC);
    }

    public static void removeComm(String inputFile){
        BufferedReader br = null;
        BufferedWriter bw = null;
        Set<String> baselineSet = null;
        try{
            br = new BufferedReader(new FileReader(inputFile));
            bw = new BufferedWriter(new FileWriter(deletedComm));
            baselineSet = new HashSet<>(Arrays.asList(dislikedComm));
            String line = null;
            Boolean flag = null;
            Set<String> sc_params_set = null;
            while ((line = br.readLine())!=null){
                flag = true;
                sc_params_set = FileUtils.getScParams(line);
                for (String param:
                     sc_params_set) {
                    if (!flag) break;
                    String[] name_value = param.split("=");
                    if (name_value[0].equals("comm")){
                        for (String disliked:
                             baselineSet) {
                            if (name_value[1].equals(disliked)){
                                flag = false;
                                break;
                            }
                        }
                    }
                }
                if (flag) {
                    bw.write(line+"\n");
                }
            }
            br.close();
            bw.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("success output : "+deletedComm);
    }

    public static void selectNextcomm(String inputFile, String next_comm){
        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            br = new BufferedReader(new FileReader(inputFile));
            bw = new BufferedWriter(new FileWriter(selectedComm));
            String line = null;
            String sc_name = null;
            Set<String> params_set = null;
            boolean flag = true;
            while ((line=br.readLine())!=null){
                ArrayList<String> sc_params = FileUtils.getNameAndParams(line);
                sc_name = sc_params.get(0);
                if (sc_name.equals("sched_switch")){
                    sc_params.remove(0);
                    params_set = new HashSet<>(sc_params);
                    for (String param:
                            params_set) {
                        if (param.equals(next_comm)){
                            flag = true;
                            break;
                        }else {
                            flag = false;
                        }
                    }
                }
                if (flag){
                    bw.write(line+"\n");
                }
            }
            br.close();
            bw.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("success output : "+selectedComm);
    }

}
