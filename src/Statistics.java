import java.io.*;
import java.util.*;
import java.util.Comparator;

public class Statistics {

    private static <K, V extends Comparable<? super V>> Comparator<Map.Entry<K,V>> comparingByValue() {
        return (Comparator<Map.Entry<K, V>> & Serializable)
                (c1, c2) -> c2.getValue().compareTo(c1.getValue());
    }

    private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(comparingByValue());

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    public static void Summery(String inputFile){
        BufferedReader br = null;
        Map<String, Integer> sc_value_map = null;
        try{
            br = new BufferedReader(new FileReader(inputFile));
            String line = null;
            String sc_name = null;
            String header_name = null;
            sc_value_map = new HashMap();
            while ((line=br.readLine())!=null){
                sc_name = FileUtils.getScName(line);
                header_name = sc_name.split("_")[0];
                if (sc_value_map.keySet().contains(header_name)){
                    int value = sc_value_map.get(header_name)+1;
                    sc_value_map.put(header_name,value);
                }else{
                    sc_value_map.put(header_name,1);
                }
            }
            br.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        if (sc_value_map!=null){
            Map<String, Integer> sortedMap = sortByValue(sc_value_map);
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(FileConfig.summery_path));
                String line =null;
                for (String header_name:
                        sortedMap.keySet() ) {
                    line = header_name+" : "+sc_value_map.get(header_name)+"\n";
                    System.out.printf(line);;
                    bw.write(line);
                }
                bw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
