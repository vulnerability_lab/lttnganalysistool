import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public abstract class Comparator {

    public abstract void buildModelImple(BufferedReader br);

    public abstract void differFuncImple(BufferedReader br, BufferedWriter bw);

    public abstract void commFuncImple(BufferedReader br, BufferedWriter bw);

    public final void buildModel(String inputFile){
        try {
            BufferedReader br = FileUtils.fileReader(inputFile);
            buildModelImple(br);
            br.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("build model success ! ! ! ");
    }

    public final void differFunc(String cleanFile, String dirtyFile){
        buildModel(cleanFile);
        try {
            BufferedReader br = FileUtils.fileReader(dirtyFile);
            BufferedWriter bw = new BufferedWriter(new FileWriter(FileConfig.DIFFER_FILE_PATH));
            differFuncImple(br, bw);
            br.close();
            bw.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("success with "+ FileConfig.DIFFER_FILE_PATH);
    }

    public final void commFunc(String cleanFile, String dirtyFile){
        buildModel(cleanFile);
        try {
            BufferedReader br = FileUtils.fileReader(dirtyFile);
            BufferedWriter bw = new BufferedWriter(new FileWriter(FileConfig.COMM_FILE_PATH));
            commFuncImple(br, bw);
            br.close();
            bw.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("success with "+ FileConfig.COMM_FILE_PATH);

    }
}
