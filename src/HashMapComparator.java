import java.io.*;
import java.util.*;

/*
* 基于HashMap的比较器实现
* 关键字：sc_name
*
* */
public class HashMapComparator extends Comparator {
    private static Map<String, Set> sc_map = null;

    @Override
    public void buildModelImple(BufferedReader br) {
        sc_map = new HashMap<>();
        try {
            String line = null;
            String sc_name = null;
            Set params_set = null;
            while ((line = br.readLine()) != null) {
                ArrayList<String> sc_params = FileUtils.getNameAndParams(line);
                sc_name = sc_params.get(0);
                sc_params.remove(0);
                params_set = new HashSet<>(sc_params);
                if (!sc_map.containsKey(sc_name)) {
                    sc_map.put(sc_name, params_set);
                } else {
                    Set<String> result = new HashSet<>();
                    Set<String> exist_params = sc_map.get(sc_name);
                    result.addAll(exist_params);
                    result.addAll(params_set);
                    sc_map.replace(sc_name, exist_params, result);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void differFuncImple(BufferedReader br, BufferedWriter bw) {
        try {
            String line = null;
            String sc_name = null;
            Set params_set = null;
            while ((line = br.readLine()) != null) {
                ArrayList<String> sc_params = FileUtils.getNameAndParams(line);
                sc_name = sc_params.get(0);
                sc_params.remove(0);
                params_set = new HashSet<>(sc_params);
                if (!sc_map.containsKey(sc_name)) {
                    bw.write(line + "\n");
                } else {
                    Set<String> exist_params = sc_map.get(sc_name);
                    params_set.removeAll(exist_params);
                    if (!params_set.isEmpty()) {
                        bw.write(line + "\n");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void commFuncImple(BufferedReader br, BufferedWriter bw) {
        try {
            String line = null;
            String sc_name = null;
            Set params_set = null;
            while ((line = br.readLine()) != null) {
                ArrayList<String> sc_params = FileUtils.getNameAndParams(line);
                sc_name = sc_params.get(0);
                sc_params.remove(0);
                params_set = new HashSet<>(sc_params);
                if (sc_map.containsKey(sc_name)) {
                    Set<String> exist_params = sc_map.get(sc_name);
                    if (exist_params.containsAll(params_set)) {
                        bw.write(line + "\n");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}