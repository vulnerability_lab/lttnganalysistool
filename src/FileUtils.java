import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileUtils {

    public static BufferedReader fileReader(String filePath){
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(filePath));
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
        return br;
    }

    /*
    * 获取sc的参数列表
    * */
    private static Set getParamsSet(String params) {
        Set scParams = new HashSet<>();
        String regex = "\\{([^}]*)\\}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(params);
        while (matcher.find()) {
            String param = matcher.group(1);
            param = param.replace(" ", "");
            if (!param.contains(",")) {
                if (param != null) {
                    scParams.add(param);
                }
            } else {
                String[] split = param.split(",");
                for (String item :
                        split) {
                    scParams.add(item);
                }
            }
        }
        return scParams;
    }

    /*
    * input : readline
    * output : sc_name and sc_params_set
    * */
    public static ArrayList<String> getNameAndParams(String line){
        ArrayList<String> sc_params = null;
        String[] params = line.split(": ");
        if (params.length < 2){
            System.out.println(line);
            return null;
        }
        int len = params[0].split(" ").length;
        String sc_name = params[0].split(" ")[len - 1];
        Set<String> params_set = getParamsSet(params[1]);
        if (sc_name!=null && params_set!= null){
            sc_params = new ArrayList<>();
            sc_params.add(sc_name);
            sc_params.addAll(params_set);
        }
        return sc_params;
    }

    public static String getScName(String line){
        String sc_name = null;
        String[] params = line.split(": ");
        int len = params[0].split(" ").length;
        sc_name = params[0].split(" ")[len - 1];
        return sc_name;
    }

    public static Set<String> getScParams(String line){
        String[] params = line.split(": ");
        Set<String> sc_params_set = getParamsSet(params[1]);
        return sc_params_set;
    }

}
