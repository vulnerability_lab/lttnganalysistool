
/*
* @author: Shouran
* @function: Comparator, Filter, Statistics
* */
public class Main {
    public static void main(String[] args) {
        Comparator c = new HashMapComparator();
        doComparison(c, "./clean.txt", "./dirty.txt");
    }

    public static void doComparison(Comparator c, String cleanFile, String dirtyFile){
        c.differFunc(cleanFile,dirtyFile);
        c.commFunc(cleanFile,dirtyFile);
    }
}
